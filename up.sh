#!/usr/bin/env bash

printf "\n Stopping containers...\n"
bash ./docker_scripts/containers_rm_all.sh


read -n1 -p "Do you want to delete docker images? [y,n]" doit
case $doit in
  y|Y)
    printf "\nRemoving images...\n"
    bash ./docker_scripts/images_rm_all.sh;;
  *)
    printf "\nNot removing images... \n"
    ;;
esac

printf "\nRunning portainer...\n"
bash ./docker_scripts/portainer_up.sh

printf "\nRunning postgres...\n"
bash ./database/postgres/run-docker.sh
