DROP TABLE IF EXISTS voucher_mvp;

DROP TYPE IF EXISTS voucher_status_mvp;
CREATE TYPE voucher_status_mvp AS ENUM ('ENABLED', 'DELETED');

CREATE TABLE public.voucher_mvp
(
    id uuid DEFAULT uuid_generate_v4 (),
    name varchar(255) NOT NULL,
    description varchar,
    promotion varchar(510),
    status voucher_status_mvp DEFAULT 'ENABLED' NOT NULL,
    id_influencer char(40),
    id_business char(40),
    units integer,
    price numeric,
    created_by varchar(255),
    updated_by varchar(255),
    created_at timestamp without time zone NOT NULL DEFAULT NOW(),
    updated_at timestamp without time zone NOT NULL DEFAULT NOW()
)
WITH (
    OIDS = FALSE
);