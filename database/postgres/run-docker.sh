#!/usr/bin/env bash

set -a
source ./database/postgres/services.envar

developmentNet=$(docker network ls -f name=development -q)

if [[ -z $developmentNet ]]; then
  printf "\nCreating development network\n"
  docker network create development
fi

printf "\nLaunching postgres container...\n"
docker-compose -f ./database/postgres/docker-compose.yml up -d --build

read -n1 -p "Do you want to create influmy_data database and tables? [y,n]" createDb
case $createDb in
  y|Y)
    printf "\nCreating database... influmy_data \n"
    docker container exec -u postgres -i postgres psql < ./database/postgres/create_db.sql

    printf "\nCreating voucher table\n"
    docker container exec -u postgres -i postgres psql influmy_data < ./database/postgres/postgres_tables/voucher.sql
    ;;
  n|N)
    printf "\nDatabase not created \n"
    ;;
  *)
    printf "\nDatabase not created \n"
    ;;
esac



