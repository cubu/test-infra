#!/usr/bin/env bash

kubectl apply -f hello-world-deployment.yaml
kubectl apply -f hello-world-service.yaml
kubectl apply -f hello-kubernetes-deployment.yaml
kubectl apply -f hello-kubernetes-service.yaml
kubectl apply -f my-ingress.yaml

echo 'Wait 5 minutes and run: kubectl get ingress my-ingress --output yaml'


