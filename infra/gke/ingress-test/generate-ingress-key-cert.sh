
openssl genrsa -out test-ingress-1.key 2048

openssl req -new -key test-ingress-1.key -out test-ingress-1.csr \
    -subj "/CN=k8s.influmy-test.com"

openssl x509 -req -days 365 -in test-ingress-1.csr -signkey test-ingress-1.key \
    -out test-ingress-1.crt
    
kubectl create secret tls my-first-secret --cert test-ingress-1.crt --key test-ingress-1.key